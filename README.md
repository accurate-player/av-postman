# Accurate.Video Postman Collection

This repository contains an exported [Postman](https://www.postman.com/) Collection and Environment to easily work with the Accurate.Video REST API.

## Import

Set up a workspace and import the collection and environment JSON files available. Refer to the [Postman docs](https://learning.postman.com/docs/getting-started/importing-and-exporting-data/) for more details.

## Environment

After importing the Example Environment, you'll need to set three [environment variables](https://learning.postman.com/docs/sending-requests/variables/), as shown below.

![Environment](environment.png)

User and password are the credentials of the user being used, for the admin user these can be found in AWS Secrets Manager. The host refers to the base path (without /api) of your Accurate.Video installation.

## Authentication

Accurate.Video uses OpenID Connect to authenticate the user with the given user/password credentials, producing two JWT tokens, an access token and a refresh token.

The access token has a default expiration time of 5 minutes, and is used for all subsequent API calls for authentication. To extend the access token, the refresh token can be used, which has a default expiration time of 30 minutes. When the access token is refreshed, the refresh token is also refreshed. If both of the tokens expire, authentication needs to be performed again to get a new access token.

All of this is handled automatically by a [pre-request script](https://learning.postman.com/docs/writing-scripts/pre-request-scripts/) configured on the collection. All the requests in the collection inherit this behavior by default, but it can be disabled if required.

![Pre-request script](pre-request-script.png)

## Using the collection

To verify that everything is set up correctly, you can trigger the API request `whoami` available in the _Auth_ folder. This shows all permissions of the configured user.

![whoami](whoami.png)

## Collection variables

Many of the API requests uses collection variables to simplify using the API. These are `ASSET_ID`, `SETTING_ID`, `STORAGE_ID`, `FILE_ID`, and `JOB_ID`. You can either set these manually, or use any of the special scripts to set these variables. You can also manually overwrite them per request, if required.

Some special request methods exists, for example in the _Asset_ folder there is a request to set `ASSET_ID` to the latest updated asset, or in the _Storage_ folder to set `STORAGE_ID` to the storage with the most assets ingested.

The JWT access and refresh tokens and their expiration time is also saved as collection variables.

## Updating

Feel free to use and add more requests to this collection, just make sure you update to the latest version and then export a new version and make a commit. Make sure don't to include any private variables on the environment or the collection.

## Links

- [Accurate.Video REST API reference](https://accurate.video/docs/api-reference/server-api/)
- [Accurate.Video REST API examples](https://accurate.video/docs/guides/rest-api-examples/)
